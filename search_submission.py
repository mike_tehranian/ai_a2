# coding=utf-8
"""
This file is your main submission that will be graded against. Only copy-paste
code on the relevant classes included here. Do not add any classes or functions
to this file that are not part of the classes that we want.
"""

from __future__ import division

import heapq
import os
import pickle


class PriorityQueue(object):
    """
    A queue structure where each element is served in order of priority.

    Elements in the queue are popped based on the priority with higher priority
    elements being served before lower priority elements.  If two elements have
    the same priority, they will be served in the order they were added to the
    queue.

    Traditionally priority queues are implemented with heaps, but there are any
    number of implementation options.

    (Hint: take a look at the module heapq)

    Attributes:
        queue (list): Nodes added to the priority queue.
        current (int): The index of the current node in the queue.
    """

    def __init__(self):
        """Initialize a new Priority Queue."""

        self.queue = []

    def pop(self):
        """
        Pop top priority node from queue.

        Returns:
            The node with the highest priority.
        """
        top_of_heap = heapq.heappop(self.queue)
        return top_of_heap

    def remove(self, node_id):
        """
        Remove a node from the queue.

        This is a hint, you might require this in ucs,
        however, if you choose not to use it, you are free to
        define your own method and not use it.

        Args:
            node_id (int): Index of node in queue.
        """
        for i in range(self.size()):
            if self.queue[i][1] == node_id:
                del self.queue[i]
                heapq.heapify(self.queue)
                break

    def __iter__(self):
        """Queue iterator."""

        return iter(sorted(self.queue))

    def __str__(self):
        """Priority Queue to string."""

        return 'PQ:%s' % self.queue

    def append(self, node):
        """
        Append a node to the queue.

        Args:
            node: Comparable Object to be added to the priority queue.
        """
        heapq.heappush(self.queue, node)

    def __contains__(self, key):
        """
        Containment Check operator for 'in'

        Args:
            key: The key to check for in the queue.

        Returns:
            True if key is found in queue, False otherwise.
        """
        return key in [n for _, n in self.queue]

    def __eq__(self, other):
        """
        Compare this Priority Queue with another Priority Queue.

        Args:
            other (PriorityQueue): Priority Queue to compare against.

        Returns:
            True if the two priority queues are equivalent.
        """
        return self == other

    def size(self):
        """
        Get the current size of the queue.

        Returns:
            Integer of number of items in queue.
        """
        return len(self.queue)

    def clear(self):
        """Reset queue to empty (no nodes)."""
        self.queue = []

    def top(self):
        """
        Get the top item in the queue.

        Returns:
            The first item stored in teh queue.
        """
        return self.queue[0]


def breadth_first_search(graph, start, goal):
    """
    Warm-up exercise: Implement breadth-first-search.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """
    if start == goal:
        return []

    parent_node = {}
    frontier = [start]
    explored = set()

    while frontier:
        current_node = frontier.pop(0)
        explored.add(current_node)
        for neighbor in graph.neighbors(current_node):
            if neighbor not in explored and neighbor not in frontier:
                parent_node[neighbor] = current_node
                if neighbor == goal:
                    # Contruct the path back to the start node
                    previous_node = goal
                    path_to_goal = [previous_node]
                    while previous_node != start:
                        previous_node = parent_node[previous_node]
                        path_to_goal = [previous_node] + path_to_goal
                    return path_to_goal
                frontier.append(neighbor)


def uniform_cost_search(graph, start, goal):
    """
    Warm-up exercise: Implement uniform_cost_search.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """
    if start == goal:
        return []

    frontier = PriorityQueue()
    frontier.append((0, start))
    explored = set()

    parent_node = {}
    cost_to_node = {}
    cost_to_node[start] = 0

    while frontier:
        current_node_cost, current_node = frontier.pop()
        if current_node == goal:
            break

        explored.add(current_node)
        for neighbor in graph.neighbors(current_node):
            # if neighbor not in explored:
            if neighbor not in explored and neighbor not in frontier:
                # First time seeing the node
                new_child_cost = current_node_cost + graph.edge[current_node][neighbor]['weight']
                cost_to_node[neighbor] = new_child_cost
                frontier.append((new_child_cost, neighbor))
                parent_node[neighbor] = current_node
            elif neighbor in frontier: # Dunder contains is overridden so this works
                incumbent_cost = cost_to_node[neighbor]
                new_child_cost = current_node_cost + graph.edge[current_node][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier.remove(neighbor) # Remove by node ID lookup
                    cost_to_node[neighbor] = new_child_cost
                    frontier.append((new_child_cost, neighbor))
                    # Overwrite a new parent node for the current node
                    parent_node[neighbor] = current_node

    previous_node = goal
    path_to_goal = [previous_node]
    while previous_node != start:
        previous_node = parent_node[previous_node]
        path_to_goal = [previous_node] + path_to_goal

    return path_to_goal


def null_heuristic(graph, v, goal):
    """
    Null heuristic used as a base line.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        v (str): Key for the node to calculate from.
        goal (str): Key for the end node to calculate to.

    Returns:
        0
    """
    return 0


def euclidean_dist_heuristic(graph, v, goal):
    """
    Warm-up exercise: Implement the euclidean distance heuristic.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        v (str): Key for the node to calculate from.
        goal (str): Key for the end node to calculate to.

    Returns:
        Euclidean distance between `v` node and `goal` node
    """
    if 'pos' in graph.node[v]:
        current_position = graph.node[v]['pos']
        goal_position = graph.node[goal]['pos']
    else:
        current_position = graph.node[v]['position']
        goal_position = graph.node[goal]['position']

    return ((current_position[0] - goal_position[0])**2 + (current_position[1] - goal_position[1])**2)**(0.5)


def a_star(graph, start, goal, heuristic=euclidean_dist_heuristic):
    """
    Warm-up exercise: Implement A* algorithm.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        heuristic: Function to determine distance heuristic.
            Default: euclidean_dist_heuristic.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """
    if start == goal:
        return []

    if goal in graph.neighbors(start):
        return [start, goal]

    frontier = PriorityQueue()
    frontier.append((heuristic(graph, start, goal), start))
    explored = set()

    parent_node = {}
    g_cost_to_node = {}
    g_cost_to_node[start] = 0

    while frontier:
        _, current_node = frontier.pop()
        if current_node == goal:
            break

        explored.add(current_node)
        for neighbor in graph.neighbors(current_node):
            if neighbor not in explored and neighbor not in frontier:
                # First time seeing the node
                new_g_cost = g_cost_to_node[current_node] + graph.edge[current_node][neighbor]['weight']
                g_cost_to_node[neighbor] = new_g_cost
                new_h_cost = heuristic(graph, neighbor, goal)

                frontier.append((new_g_cost + new_h_cost, neighbor))
                parent_node[neighbor] = current_node
            elif neighbor in frontier: # Dunder contains is overridden so this works
                # h costs cancel out so they can be ignored when comparing the incumbent and new f costs
                incumbent_cost = g_cost_to_node[neighbor]
                new_child_cost = g_cost_to_node[current_node] + graph.edge[current_node][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier.remove(neighbor) # Remove by node ID lookup
                    g_cost_to_node[neighbor] = new_child_cost
                    frontier.append((new_child_cost + heuristic(graph, neighbor, goal), neighbor))
                    # Overwrite a new parent node for the current node
                    parent_node[neighbor] = current_node

    previous_node = goal
    path_to_goal = [previous_node]
    while previous_node != start:
        previous_node = parent_node[previous_node]
        path_to_goal = [previous_node] + path_to_goal

    return path_to_goal


def bidirectional_ucs(graph, start, goal):
    """
    Exercise 1: Bidirectional Search.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """
    # Loosely based on: http://planning.cs.uiuc.edu/node50.html
    if start == goal:
        return []

    explored_start = set()
    explored_goal = set()

    frontier_start = PriorityQueue()
    frontier_goal = PriorityQueue()
    frontier_start.append((0, start))
    frontier_goal.append((0, goal))

    cost_to_node_from_start = {start: 0}
    cost_to_node_from_goal = {goal: 0}

    parent_node_to_goal = {}
    parent_node_to_start = {}

    minimum_total_path = []
    minimum_total_cost = float('inf')

    while frontier_start and frontier_goal:
        cost_start, node_start = frontier_start.pop()
        cost_goal, node_goal = frontier_goal.pop()

        if cost_start + cost_goal >= minimum_total_cost:
            # End of search; stop once we can not get any lower cost
            return minimum_total_path

        # Start Node
        explored_start.add(node_start)
        for neighbor in graph.neighbors(node_start):
            # if neighbor not in explored:
            if neighbor not in explored_start and neighbor not in frontier_start:
                # First time seeing the node
                new_child_cost = cost_start + graph.edge[node_start][neighbor]['weight']
                cost_to_node_from_start[neighbor] = new_child_cost
                frontier_start.append((new_child_cost, neighbor))
                parent_node_to_goal[neighbor] = node_start
            elif neighbor in frontier_start: # Dunder contains is overridden so this works
                incumbent_cost = cost_to_node_from_start[neighbor]
                new_child_cost = cost_start + graph.edge[node_start][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier_start.remove(neighbor) # Remove by node ID lookup
                    cost_to_node_from_start[neighbor] = new_child_cost
                    frontier_start.append((new_child_cost, neighbor))
                    # Overwrite a new parent node for the current node
                    parent_node_to_goal[neighbor] = node_start
            if neighbor in explored_goal or neighbor in frontier_goal:
                # Calculate the cost to the complete the total path from start to goal
                new_total_path_cost = cost_to_node_from_start[neighbor] + \
                    cost_to_node_from_goal[neighbor]
                if new_total_path_cost < minimum_total_cost:
                    # Keep track of the minimum total cost
                    minimum_total_cost = new_total_path_cost
                    if neighbor == goal:
                        # # Edge case: when the bidirectional search degenerates without overlap
                        minimum_total_path = create_path_when_no_overlap(
                                              start, goal, parent_node_to_goal, False)
                    else:
                        minimum_total_path = create_path_from_intersecting_paths(start, goal, parent_node_to_goal, parent_node_to_start, neighbor)

        # Goal Node
        explored_goal.add(node_goal)
        for neighbor in graph.neighbors(node_goal):
            # if neighbor not in explored:
            if neighbor not in explored_goal and neighbor not in frontier_goal:
                # First time seeing the node
                new_child_cost = cost_goal + graph.edge[node_goal][neighbor]['weight']
                cost_to_node_from_goal[neighbor] = new_child_cost
                frontier_goal.append((new_child_cost, neighbor))
                parent_node_to_start[neighbor] = node_goal
            elif neighbor in frontier_goal: # Dunder contains is overridden so this works
                incumbent_cost = cost_to_node_from_goal[neighbor]
                new_child_cost = cost_goal + graph.edge[node_goal][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier_goal.remove(neighbor) # Remove by node ID lookup
                    cost_to_node_from_goal[neighbor] = new_child_cost
                    frontier_goal.append((new_child_cost, neighbor))
                    # Overwrite a new parent node for the current node
                    parent_node_to_start[neighbor] = node_goal
            if neighbor in explored_start or neighbor in frontier_start:
                # Calculate the cost to the complete the total path from start to goal
                new_total_path_cost = cost_to_node_from_start[neighbor] + \
                    cost_to_node_from_goal[neighbor]
                if new_total_path_cost < minimum_total_cost:
                    # Keep track of the minimum total cost
                    minimum_total_cost = new_total_path_cost
                    if neighbor == start:
                        # Edge case: when the bidirectional search degenerates without overlap
                        minimum_total_path = create_path_when_no_overlap(
                                              goal, start, parent_node_to_start, True)
                    else:
                        minimum_total_path = create_path_from_intersecting_paths(start, goal, parent_node_to_goal, parent_node_to_start, neighbor)


def create_path_from_intersecting_paths(start, goal, parent_node_to_goal, parent_node_to_start, neighbor):
    # Generate path from start to neighbor (inclusive)
    previous_node = neighbor
    path_from_start = [previous_node]
    while previous_node != start:
        previous_node = parent_node_to_goal[previous_node]
        path_from_start = [previous_node] + path_from_start

    # Generate path from neighbor (exclusive) to goal
    previous_node = neighbor
    path_from_goal = []
    while previous_node != goal:
        previous_node = parent_node_to_start[previous_node]
        path_from_goal = path_from_goal + [previous_node]

    # Finally, keep track of the minimum total path
    minimum_total_path = path_from_start + path_from_goal
    return minimum_total_path

def create_path_when_no_overlap(start, goal, parent_node_to_goal, reverse):
    path_to_goal = []
    previous_node = goal
    while previous_node != start:
        path_to_goal = [previous_node] + path_to_goal
        previous_node = parent_node_to_goal[previous_node]
    # Keep track of the minimum total path
    minimum_total_path = [start] + path_to_goal
    if reverse:
        return list(reversed(minimum_total_path))
    else:
        return minimum_total_path


def bidirectional_a_star(graph, start, goal,
                         heuristic=euclidean_dist_heuristic):
    """
    Exercise 2: Bidirectional A*.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        heuristic: Function to determine distance heuristic.
            Default: euclidean_dist_heuristic.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """
    if start == goal:
        return []

    if goal in graph.neighbors(start):
        return [start, goal]

    explored_start = set()
    explored_goal = set()

    frontier_start = PriorityQueue()
    frontier_goal = PriorityQueue()
    frontier_start.append((0, start))
    frontier_goal.append((0, goal))

    cost_to_node_from_start = {start: 0}
    cost_to_node_from_goal = {goal: 0}

    parent_node_to_goal = {}
    parent_node_to_start = {}

    minimum_total_path = []
    minimum_total_cost = float('inf')

    h_cost_start_to_goal = heuristic(graph, start, goal)

    while frontier_start and frontier_goal:
        cost_start, node_start = frontier_start.pop()
        cost_goal, node_goal = frontier_goal.pop()

        # TODO another option for break condition:
        # topf + topr <= mu + (.5 * (h(goal_n,goal) - h(start_n, goal)) + .5(h(start, start_n))
        if cost_start + cost_goal >= minimum_total_cost + h_cost_start_to_goal:
            # End of search; stop once we can not get any lower cost
            return minimum_total_path

        # Start Node
        explored_start.add(node_start)
        for neighbor in graph.neighbors(node_start):
            if neighbor not in explored_start and neighbor not in frontier_start:
                # First time seeing the node
                new_child_cost = cost_to_node_from_start[node_start] + graph.edge[node_start][neighbor]['weight']
                cost_to_node_from_start[neighbor] = new_child_cost

                new_h_cost_to_goal = heuristic(graph, neighbor, goal)
                new_h_cost_to_start = heuristic(graph, start, neighbor)
                # From Goldberg, et al., Page 15
                new_h_total_average_cost = 0.5 * (new_h_cost_to_goal - new_h_cost_to_start) + 0.5 * h_cost_start_to_goal
                frontier_start.append((new_child_cost + new_h_total_average_cost, neighbor))

                parent_node_to_goal[neighbor] = node_start
            elif neighbor in frontier_start: # Dunder contains is overridden so this works
                incumbent_cost = cost_to_node_from_start[neighbor]
                new_child_cost = cost_to_node_from_start[node_start] + graph.edge[node_start][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier_start.remove(neighbor) # Remove by node ID lookup
                    cost_to_node_from_start[neighbor] = new_child_cost

                    new_h_cost_to_goal = heuristic(graph, neighbor, goal)
                    new_h_cost_to_start = heuristic(graph, start, neighbor)
                    # From Goldberg, et al., Page 15
                    new_h_total_average_cost = 0.5 * (new_h_cost_to_goal - new_h_cost_to_start) + 0.5 * h_cost_start_to_goal
                    frontier_start.append((new_child_cost + new_h_total_average_cost, neighbor))

                    # Overwrite a new parent node for the current node
                    parent_node_to_goal[neighbor] = node_start
            if neighbor in explored_goal:
                # Calculate the cost to the complete the total path from start to goal
                new_total_path_cost = cost_to_node_from_start[neighbor] + \
                    cost_to_node_from_goal[neighbor]
                if new_total_path_cost < minimum_total_cost:
                    # Keep track of the minimum total cost
                    minimum_total_cost = new_total_path_cost
                    if neighbor == goal:
                        minimum_total_path = create_path_when_no_overlap(start, goal, parent_node_to_goal, False)
                    else:
                        minimum_total_path = create_path_from_intersecting_paths(start, goal, parent_node_to_goal, parent_node_to_start, neighbor)

        # Goal Node
        explored_goal.add(node_goal)
        for neighbor in graph.neighbors(node_goal):
            if neighbor not in explored_goal and neighbor not in frontier_goal:
                # First time seeing the node
                new_child_cost = cost_to_node_from_goal[node_goal] + graph.edge[node_goal][neighbor]['weight']
                cost_to_node_from_goal[neighbor] = new_child_cost

                new_h_cost_to_goal = heuristic(graph, neighbor, goal)
                new_h_cost_to_start = heuristic(graph, start, neighbor)
                # From Goldberg, et al., Page 15
                new_h_total_average_cost =  0.5 * (new_h_cost_to_start - new_h_cost_to_goal) + 0.5 * h_cost_start_to_goal
                frontier_goal.append((new_child_cost + new_h_total_average_cost, neighbor))

                parent_node_to_start[neighbor] = node_goal
            elif neighbor in frontier_goal: # Dunder contains is overridden so this works
                incumbent_cost = cost_to_node_from_goal[neighbor]
                new_child_cost = cost_to_node_from_goal[node_goal] + graph.edge[node_goal][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier_goal.remove(neighbor) # Remove by node ID lookup
                    cost_to_node_from_goal[neighbor] = new_child_cost

                    new_h_cost_to_goal = heuristic(graph, neighbor, goal)
                    new_h_cost_to_start = heuristic(graph, start, neighbor)
                    # From Goldberg, et al., Page 15
                    new_h_total_average_cost =  0.5 * (new_h_cost_to_start - new_h_cost_to_goal) + 0.5 * h_cost_start_to_goal
                    frontier_goal.append((new_child_cost + new_h_total_average_cost, neighbor))

                    # Overwrite a new parent node for the current node
                    parent_node_to_start[neighbor] = node_goal
            if neighbor in explored_start:
                # Calculate the cost to the complete the total path from start to goal
                new_total_path_cost = cost_to_node_from_start[neighbor] + \
                    cost_to_node_from_goal[neighbor]
                if new_total_path_cost < minimum_total_cost:
                    # Keep track of the minimum total cost
                    minimum_total_cost = new_total_path_cost
                    if neighbor == start:
                        # Edge case: when the bidirectional search degenerates without overlap
                        minimum_total_path = create_path_when_no_overlap(goal, start, parent_node_to_start, True)

                    else:
                        minimum_total_path = create_path_from_intersecting_paths(start, goal, parent_node_to_goal, parent_node_to_start, neighbor)

def find_max_cost(minimum_total_cost_one_two, minimum_total_cost_one_three, minimum_total_cost_two_three,
                   minimum_total_path_one_two, minimum_total_path_one_three, minimum_total_path_two_three):
    max_cost = max(minimum_total_cost_one_two, minimum_total_cost_one_three, minimum_total_cost_two_three)

    if max_cost == minimum_total_cost_one_two:
        return minimum_total_path_one_three[:-1] + minimum_total_path_two_three[::-1]
    if max_cost == minimum_total_cost_one_three:
        return minimum_total_path_one_two + minimum_total_path_two_three[1:]
    if max_cost == minimum_total_cost_two_three:
        return minimum_total_path_one_three[::-1] + minimum_total_path_one_two[1:]


def check_stop_criteria(cost_one, cost_two, cost_three,
                            minimum_total_cost_one_two, minimum_total_cost_one_three,
                            minimum_total_cost_two_three, minimum_total_path_one_two,
                            minimum_total_path_one_three, minimum_total_path_two_three,
                            keep_going_one, keep_going_two, keep_going_three):

    if minimum_total_cost_one_two != float('inf') and minimum_total_cost_one_three != float('inf'):
        if cost_one + cost_two >= minimum_total_cost_one_two:
            if cost_one + cost_three >= minimum_total_cost_one_three:
                keep_going_one = False
                if cost_two + cost_three >= minimum_total_cost_one_two:
                    if cost_two + cost_three >= minimum_total_cost_one_three:
                        return keep_going_one, keep_going_two, keep_going_three, \
                                find_max_cost(minimum_total_cost_one_two,
                                minimum_total_cost_one_three, minimum_total_cost_two_three,
                                   minimum_total_path_one_two, minimum_total_path_one_three,
                                   minimum_total_path_two_three)

    if minimum_total_cost_one_two != float('inf') and minimum_total_cost_two_three != float('inf'):
        if cost_one + cost_two >= minimum_total_cost_one_two:
            if cost_two + cost_three >= minimum_total_cost_two_three:
                keep_going_two = False
                if cost_one + cost_three >= minimum_total_cost_one_two:
                    if cost_one + cost_three >= minimum_total_cost_two_three:
                        return keep_going_one, keep_going_two, keep_going_three, \
                                find_max_cost(minimum_total_cost_one_two,
                                minimum_total_cost_one_three, minimum_total_cost_two_three,
                                   minimum_total_path_one_two, minimum_total_path_one_three,
                                   minimum_total_path_two_three)

    if minimum_total_cost_one_three != float('inf') and minimum_total_cost_two_three != float('inf'):
        if cost_one + cost_three >= minimum_total_cost_one_three:
            if cost_two + cost_three >= minimum_total_cost_two_three:
                keep_going_three = False
                if cost_one + cost_two >= minimum_total_cost_one_three:
                    if cost_one + cost_two >= minimum_total_cost_two_three:
                        return keep_going_one, keep_going_two, keep_going_three, \
                                find_max_cost(minimum_total_cost_one_two,
                                minimum_total_cost_one_three, minimum_total_cost_two_three,
                                   minimum_total_path_one_two, minimum_total_path_one_three,
                                   minimum_total_path_two_three)

    return keep_going_one, keep_going_two, keep_going_three, None

def tridirectional_search(graph, goals):
    """
    Exercise 3: Tridirectional UCS Search

    See README.MD for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        goals (list): Key values for the 3 goals

    Returns:
        The best path as a list from one of the goal nodes (including both of
        the other goal nodes).
    """
    if len(goals) > len(set(goals)):
        return []

    one = goals[0]
    two = goals[1]
    three = goals[2]

    explored_one = set()
    explored_two = set()
    explored_three = set()

    frontier_one = PriorityQueue()
    frontier_two = PriorityQueue()
    frontier_three = PriorityQueue()
    frontier_one.append((0, goals[0]))
    frontier_two.append((0, goals[1]))
    frontier_three.append((0, goals[2]))

    cost_to_node_one = {goals[0]: 0}
    cost_to_node_two = {goals[1]: 0}
    cost_to_node_three = {goals[2]: 0}

    parent_node_one = {}
    parent_node_two = {}
    parent_node_three = {}

    minimum_total_path_one_two = []
    minimum_total_path_one_three = []
    minimum_total_path_two_three = []
    minimum_total_path = []

    minimum_total_cost_one_two = float('inf')
    minimum_total_cost_one_three = float('inf')
    minimum_total_cost_two_three = float('inf')
    minimum_total_cost = float('inf')

    keep_going_one = True
    keep_going_two = True
    keep_going_three = True

    # while frontier_one and frontier_two and frontier_three:
    while True:
        if frontier_one.size() > 0:
            cost_one, node_one = frontier_one.pop()
        if frontier_two.size() > 0:
            cost_two, node_two = frontier_two.pop()
        if frontier_three.size() > 0:
            cost_three, node_three = frontier_three.pop()

        if minimum_total_cost_one_two != float('inf') and minimum_total_cost_one_three != float('inf') \
                and minimum_total_cost_two_three != float('inf'):
                    # TODO check this too
                    pass

        keep_going_one, keep_going_two, keep_going_three, optimal_path = check_stop_criteria(
                            cost_one, cost_two, cost_three,
                            minimum_total_cost_one_two, minimum_total_cost_one_three,
                            minimum_total_cost_two_three, minimum_total_path_one_two,
                            minimum_total_path_one_three, minimum_total_path_two_three,
                            keep_going_one, keep_going_two, keep_going_three)
        if optimal_path is not None:
            return optimal_path

        # First Node
        if keep_going_one:
            explored_one.add(node_one)
            for neighbor in graph.neighbors(node_one):
                if neighbor not in explored_one and neighbor not in frontier_one:
                    # First time seeing the node
                    new_child_cost = cost_to_node_one[node_one] + graph.edge[node_one][neighbor]['weight']
                    cost_to_node_one[neighbor] = new_child_cost
                    frontier_one.append((new_child_cost, neighbor))
                    parent_node_one[neighbor] = node_one
                elif neighbor in frontier_one: # Dunder contains is overridden so this works
                    incumbent_cost = cost_to_node_one[neighbor]
                    new_child_cost = cost_to_node_one[node_one] + graph.edge[node_one][neighbor]['weight']
                    if new_child_cost < incumbent_cost:
                        frontier_one.remove(neighbor) # Remove by node ID lookup
                        cost_to_node_one[neighbor] = new_child_cost
                        frontier_one.append((new_child_cost, neighbor))
                        # Overwrite a new parent node for the current node
                        parent_node_one[neighbor] = node_one
                if neighbor in explored_two:
                    # Calculate the cost to the complete the total path from one to two
                    new_total_path_cost = cost_to_node_one[neighbor] + \
                        cost_to_node_two[neighbor]
                    if new_total_path_cost < minimum_total_cost_one_two:
                        # Keep track of the minimum total cost
                        minimum_total_cost_one_two = new_total_path_cost
                        if neighbor == two:
                            # Edge case: when the bidirectional search degenerates without overlap
                            minimum_total_path_one_two = create_path_when_no_overlap(
                                                  one, two, parent_node_one, False)
                        else:
                            minimum_total_path_one_two = create_path_from_intersecting_paths(one, two, parent_node_one, parent_node_two, neighbor)

                if neighbor in explored_three:
                    # Calculate the cost to the complete the total path from one to three
                    new_total_path_cost = cost_to_node_one[neighbor] + \
                        cost_to_node_three[neighbor]
                    if new_total_path_cost < minimum_total_cost_one_three:
                        # Keep track of the minimum total cost
                        minimum_total_cost_one_three = new_total_path_cost
                        if neighbor == three:
                            # Edge case: when the bidirectional search degenerates without overlap
                            minimum_total_path_one_three = create_path_when_no_overlap(
                                                  one, three, parent_node_one, False)
                        else:
                            minimum_total_path_one_three = create_path_from_intersecting_paths(one, three, parent_node_one, parent_node_three, neighbor)


        keep_going_one, keep_going_two, keep_going_three, optimal_path = check_stop_criteria(
                            cost_one, cost_two, cost_three,
                            minimum_total_cost_one_two, minimum_total_cost_one_three,
                            minimum_total_cost_two_three, minimum_total_path_one_two,
                            minimum_total_path_one_three, minimum_total_path_two_three,
                            keep_going_one, keep_going_two, keep_going_three)
        if optimal_path is not None:
            return optimal_path

        # Second Node
        if keep_going_two:
            explored_two.add(node_two)
            for neighbor in graph.neighbors(node_two):
                if neighbor not in explored_two and neighbor not in frontier_two:
                    # First time seeing the node
                    new_child_cost = cost_to_node_two[node_two] + graph.edge[node_two][neighbor]['weight']
                    cost_to_node_two[neighbor] = new_child_cost
                    frontier_two.append((new_child_cost, neighbor))
                    parent_node_two[neighbor] = node_two
                elif neighbor in frontier_two: # Dunder contains is overridden so this works
                    incumbent_cost = cost_to_node_two[neighbor]
                    new_child_cost = cost_to_node_two[node_two] + graph.edge[node_two][neighbor]['weight']
                    if new_child_cost < incumbent_cost:
                        frontier_two.remove(neighbor) # Remove by node ID lookup
                        cost_to_node_two[neighbor] = new_child_cost
                        frontier_two.append((new_child_cost, neighbor))
                        # Overwrite a new parent node for the current node
                        parent_node_two[neighbor] = node_two
                if neighbor in explored_one:
                    # Calculate the cost to the complete the total path from two to one
                    new_total_path_cost = cost_to_node_one[neighbor] + \
                        cost_to_node_two[neighbor]
                    if new_total_path_cost < minimum_total_cost_one_two:
                        # Keep track of the minimum total cost
                        minimum_total_cost_one_two = new_total_path_cost
                        if neighbor == one:
                            # Edge case: when the bidirectional search degenerates without overlap
                            minimum_total_path_one_two = create_path_when_no_overlap(
                                                  two, one, parent_node_two, True)
                        else:
                            minimum_total_path_one_two = create_path_from_intersecting_paths(one, two, parent_node_one, parent_node_two, neighbor)

                if neighbor in explored_three:
                    # Calculate the cost to the complete the total path from one to three
                    new_total_path_cost = cost_to_node_two[neighbor] + \
                        cost_to_node_three[neighbor]
                    if new_total_path_cost < minimum_total_cost_two_three:
                        # Keep track of the minimum total cost
                        minimum_total_cost_two_three = new_total_path_cost
                        if neighbor == three:
                            # Edge case: when the bidirectional search degenerates without overlap
                            minimum_total_path_two_three = create_path_when_no_overlap(
                                                  two, three, parent_node_two, False)
                        else:
                            minimum_total_path_two_three = create_path_from_intersecting_paths(two, three, parent_node_two, parent_node_three, neighbor)

        keep_going_one, keep_going_two, keep_going_three, optimal_path = check_stop_criteria(
                            cost_one, cost_two, cost_three,
                            minimum_total_cost_one_two, minimum_total_cost_one_three,
                            minimum_total_cost_two_three, minimum_total_path_one_two,
                            minimum_total_path_one_three, minimum_total_path_two_three,
                            keep_going_one, keep_going_two, keep_going_three)
        if optimal_path is not None:
            return optimal_path

        # Third Node
        if keep_going_three:
            explored_three.add(node_three)
            for neighbor in graph.neighbors(node_three):
                if neighbor not in explored_three and neighbor not in frontier_three:
                    # First time seeing the node
                    new_child_cost = cost_to_node_three[node_three] + graph.edge[node_three][neighbor]['weight']
                    cost_to_node_three[neighbor] = new_child_cost
                    frontier_three.append((new_child_cost, neighbor))
                    parent_node_three[neighbor] = node_three
                elif neighbor in frontier_three: # Dunder contains is overridden so this works
                    incumbent_cost = cost_to_node_three[neighbor]
                    new_child_cost = cost_to_node_three[node_three] + graph.edge[node_three][neighbor]['weight']
                    if new_child_cost < incumbent_cost:
                        frontier_three.remove(neighbor) # Remove by node ID lookup
                        cost_to_node_three[neighbor] = new_child_cost
                        frontier_three.append((new_child_cost, neighbor))
                        # Overwrite a new parent node for the current node
                        parent_node_three[neighbor] = node_three
                if neighbor in explored_one:
                    # Calculate the cost to the complete the total path from three to one
                    new_total_path_cost = cost_to_node_one[neighbor] + \
                        cost_to_node_three[neighbor]
                    if new_total_path_cost < minimum_total_cost_one_three:
                        # Keep track of the minimum total cost
                        minimum_total_cost_one_three = new_total_path_cost
                        if neighbor == one:
                            # Edge case: when the bidirectional search degenerates without overlap
                            minimum_total_path_one_three = create_path_when_no_overlap(
                                                  three, one, parent_node_three, True)
                        else:
                            minimum_total_path_one_three = create_path_from_intersecting_paths(one, three, parent_node_one, parent_node_three, neighbor)

                if neighbor in explored_two:
                    # Calculate the cost to the complete the total path from two to three
                    new_total_path_cost = cost_to_node_two[neighbor] + \
                        cost_to_node_three[neighbor]
                    if new_total_path_cost < minimum_total_cost_two_three:
                        # Keep track of the minimum total cost
                        minimum_total_cost_two_three = new_total_path_cost
                        if neighbor == two:
                            # Edge case: when the bidirectional search degenerates without overlap
                            minimum_total_path_two_three = create_path_when_no_overlap(
                                                  three, two, parent_node_three, True)
                        else:
                            minimum_total_path_two_three = create_path_from_intersecting_paths(two, three, parent_node_two, parent_node_three, neighbor)


def tridirectional_upgraded(graph, goals, heuristic=euclidean_dist_heuristic):
    """
    Exercise 3: Upgraded Tridirectional Search

    See README.MD for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        goals (list): Key values for the 3 goals
        heuristic: Function to determine distance heuristic.
            Default: euclidean_dist_heuristic.

    Returns:
        The best path as a list from one of the goal nodes (including both of
        the other goal nodes).
    """
    # TODO MDT - create booleans for each goal and its two direction to each node
    # call it - keep going - which is a check to tell each goal whterh or not it should
    # keep going in each of the directions towards the other goals. The check is based upon
    # the distance found so far. Thus, it's possible later in the search that a goal does not
    # advance forward at all if all of its costs are too high
    # TODO: finish this function
    if len(goals) > len(set(goals)):
        return []

    one = goals[0]
    two = goals[1]
    three = goals[2]

    explored_one = set()
    explored_two = set()
    explored_three = set()

    frontier_one = PriorityQueue()
    frontier_two = PriorityQueue()
    frontier_three = PriorityQueue()
    frontier_one.append((0, goals[0]))
    frontier_two.append((0, goals[1]))
    frontier_three.append((0, goals[2]))

    cost_to_node_one = {goals[0]: 0}
    cost_to_node_two = {goals[1]: 0}
    cost_to_node_three = {goals[2]: 0}

    parent_node_one = {}
    parent_node_two = {}
    parent_node_three = {}

    minimum_total_path_one_two = []
    minimum_total_path_one_three = []
    minimum_total_path_two_three = []
    minimum_total_path = []

    minimum_total_cost_one_two = float('inf')
    minimum_total_cost_one_three = float('inf')
    minimum_total_cost_two_three = float('inf')
    minimum_total_cost = float('inf')

    minimum_total_path_one_two, minimum_total_cost_one_two = pairwise_a_star(graph, one, two)
    minimum_total_path_one_three, minimum_total_cost_one_three = pairwise_a_star(graph, one, three)
    minimum_total_path_two_three, minimum_total_cost_two_three = pairwise_a_star(graph, two, three)

    sum_first_pair = minimum_total_cost_one_two + minimum_total_cost_one_three
    sum_second_pair = minimum_total_cost_one_two + minimum_total_cost_two_three
    sum_third_pair = minimum_total_cost_one_three + minimum_total_cost_two_three

    max_cost = max(minimum_total_cost_one_two, minimum_total_cost_one_three, minimum_total_cost_two_three)

    if max_cost == minimum_total_cost_one_two:
        return minimum_total_path_one_three[:-1] + minimum_total_path_two_three[::-1]
    if max_cost == minimum_total_cost_one_three:
        return minimum_total_path_one_two + minimum_total_path_two_three[1:]
    if max_cost == minimum_total_cost_two_three:
        return minimum_total_path_one_three[::-1] + minimum_total_path_one_two[1:]

def pairwise_a_star(graph, start, goal):
    if goal in graph.neighbors(start):
        return [start, goal], graph.edge[start][goal]['weight']

    explored_start = set()
    explored_goal = set()

    frontier_start = PriorityQueue()
    frontier_goal = PriorityQueue()
    frontier_start.append((0, start))
    frontier_goal.append((0, goal))

    cost_to_node_from_start = {start: 0}
    cost_to_node_from_goal = {goal: 0}

    parent_node_to_goal = {}
    parent_node_to_start = {}

    minimum_total_path = []
    minimum_total_cost = float('inf')

    h_cost_start_to_goal = euclidean_dist_heuristic(graph, start, goal)

    while frontier_start and frontier_goal:
        cost_start, node_start = frontier_start.pop()
        cost_goal, node_goal = frontier_goal.pop()

        # TODO another option for break condition:
        # topf + topr <= mu + (.5 * (h(goal_n,goal) - h(start_n, goal)) + .5(h(start, start_n))
        if cost_start + cost_goal >= minimum_total_cost + h_cost_start_to_goal:
            # End of search; stop once we can not get any lower cost
            return minimum_total_path, minimum_total_cost

        # Start Node
        explored_start.add(node_start)
        for neighbor in graph.neighbors(node_start):
            if neighbor not in explored_start and neighbor not in frontier_start:
                # First time seeing the node
                new_child_cost = cost_to_node_from_start[node_start] + graph.edge[node_start][neighbor]['weight']
                cost_to_node_from_start[neighbor] = new_child_cost

                new_h_cost_to_goal = euclidean_dist_heuristic(graph, neighbor, goal)
                new_h_cost_to_start = euclidean_dist_heuristic(graph, start, neighbor)
                # From Goldberg, et al., Page 15
                new_h_total_average_cost = 0.5 * (new_h_cost_to_goal - new_h_cost_to_start) + 0.5 * h_cost_start_to_goal
                frontier_start.append((new_child_cost + new_h_total_average_cost, neighbor))

                parent_node_to_goal[neighbor] = node_start
            elif neighbor in frontier_start: # Dunder contains is overridden so this works
                incumbent_cost = cost_to_node_from_start[neighbor]
                new_child_cost = cost_to_node_from_start[node_start] + graph.edge[node_start][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier_start.remove(neighbor) # Remove by node ID lookup
                    cost_to_node_from_start[neighbor] = new_child_cost

                    new_h_cost_to_goal = euclidean_dist_heuristic(graph, neighbor, goal)
                    new_h_cost_to_start = euclidean_dist_heuristic(graph, start, neighbor)
                    # From Goldberg, et al., Page 15
                    new_h_total_average_cost = 0.5 * (new_h_cost_to_goal - new_h_cost_to_start) + 0.5 * h_cost_start_to_goal
                    frontier_start.append((new_child_cost + new_h_total_average_cost, neighbor))

                    # Overwrite a new parent node for the current node
                    parent_node_to_goal[neighbor] = node_start
            if neighbor in explored_goal:
                # Calculate the cost to the complete the total path from start to goal
                new_total_path_cost = cost_to_node_from_start[neighbor] + \
                    cost_to_node_from_goal[neighbor]
                if new_total_path_cost < minimum_total_cost:
                    # Keep track of the minimum total cost
                    minimum_total_cost = new_total_path_cost
                    if neighbor == goal:
                        minimum_total_path = create_path_when_no_overlap(start, goal, parent_node_to_goal, False)
                    else:
                        minimum_total_path = create_path_from_intersecting_paths(start, goal, parent_node_to_goal, parent_node_to_start, neighbor)

        # Goal Node
        explored_goal.add(node_goal)
        for neighbor in graph.neighbors(node_goal):
            if neighbor not in explored_goal and neighbor not in frontier_goal:
                # First time seeing the node
                new_child_cost = cost_to_node_from_goal[node_goal] + graph.edge[node_goal][neighbor]['weight']
                cost_to_node_from_goal[neighbor] = new_child_cost

                new_h_cost_to_goal = euclidean_dist_heuristic(graph, neighbor, goal)
                new_h_cost_to_start = euclidean_dist_heuristic(graph, start, neighbor)
                # From Goldberg, et al., Page 15
                new_h_total_average_cost =  0.5 * (new_h_cost_to_start - new_h_cost_to_goal) + 0.5 * h_cost_start_to_goal
                frontier_goal.append((new_child_cost + new_h_total_average_cost, neighbor))

                parent_node_to_start[neighbor] = node_goal
            elif neighbor in frontier_goal: # Dunder contains is overridden so this works
                incumbent_cost = cost_to_node_from_goal[neighbor]
                new_child_cost = cost_to_node_from_goal[node_goal] + graph.edge[node_goal][neighbor]['weight']
                if new_child_cost < incumbent_cost:
                    frontier_goal.remove(neighbor) # Remove by node ID lookup
                    cost_to_node_from_goal[neighbor] = new_child_cost

                    new_h_cost_to_goal = euclidean_dist_heuristic(graph, neighbor, goal)
                    new_h_cost_to_start = euclidean_dist_heuristic(graph, start, neighbor)
                    # From Goldberg, et al., Page 15
                    new_h_total_average_cost =  0.5 * (new_h_cost_to_start - new_h_cost_to_goal) + 0.5 * h_cost_start_to_goal
                    frontier_goal.append((new_child_cost + new_h_total_average_cost, neighbor))

                    # Overwrite a new parent node for the current node
                    parent_node_to_start[neighbor] = node_goal
            if neighbor in explored_start:
                # Calculate the cost to the complete the total path from start to goal
                new_total_path_cost = cost_to_node_from_start[neighbor] + \
                    cost_to_node_from_goal[neighbor]
                if new_total_path_cost < minimum_total_cost:
                    # Keep track of the minimum total cost
                    minimum_total_cost = new_total_path_cost
                    if neighbor == start:
                        # Edge case: when the bidirectional search degenerates without overlap
                        minimum_total_path = create_path_when_no_overlap(goal, start, parent_node_to_start, True)

                    else:
                        minimum_total_path = create_path_from_intersecting_paths(start, goal, parent_node_to_goal, parent_node_to_start, neighbor)


def return_your_name():
    """Return your name from this function"""
    return "Michael Tehranian"


# Extra Credit: Your best search method for the race
def custom_search(graph, start, goal, data=None):
    """
    Race!: Implement your best search algorithm here to compete against the
    other student agents.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        data :  Data used in the custom search.
            Default: None.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    # TODO: finish this function!
    raise NotImplementedError


def load_data():
    """
    Loads data from data.pickle and return the data object that is passed to
    the custom_search method.

    Will be called only once. Feel free to modify.

    Returns:
         The data loaded from the pickle file.
    """

    dir_name = os.path.dirname(os.path.realpath(__file__))
    pickle_file_path = os.path.join(dir_name, "data.pickle")
    data = pickle.load(open(pickle_file_path, 'rb'))
    return data
